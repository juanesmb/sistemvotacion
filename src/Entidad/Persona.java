package Entidad;


import java.time.LocalDateTime;

public class Persona {

    private long cedula;

    private String nombre;

    private LocalDateTime fechaNacimiento;

    private boolean esJurado;

    private boolean esSufragante;

    private short id_Municipio_inscripcion;
    
    private String email;

    public Persona() {
    }

    public Persona(long cedula, String nombre, LocalDateTime fechaNacimiento, short id_Municipio_inscripcion, String email) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.id_Municipio_inscripcion = id_Municipio_inscripcion;
        this.email = email;
    }
    
    
    
    

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDateTime getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDateTime fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public boolean isEsJurado() {
        return esJurado;
    }

    public void setEsJurado(boolean esJurado) {
        this.esJurado = esJurado;
    }

    public boolean isEsSufragante() {
        return esSufragante;
    }

    public void setEsSufragante(boolean esSufragante) {
        this.esSufragante = esSufragante;
    }

    public short getId_Municipio_inscripcion() {
        return id_Municipio_inscripcion;
    }

    public void setId_Municipio_inscripcion(short id_Municipio_inscripcion) {
        this.id_Municipio_inscripcion = id_Municipio_inscripcion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + ", fechaNacimiento=" + fechaNacimiento.toString() + ", esJurado=" + esJurado + ", esSufragante=" + esSufragante + ", id_Municipio_inscripcion=" + id_Municipio_inscripcion + ", email=" + email + '}';
    }

    
}
