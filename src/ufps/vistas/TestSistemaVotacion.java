package ufps.vistas;
import Negocio.SistemaVotacion;


public class TestSistemaVotacion {
    
    public static void main(String[] args) {
        String urlDptos = "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/departamento.csv";
        String urlMun = "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/municipios.csv";
        String urlPer = "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/personas.csv";
        SistemaVotacion s = new SistemaVotacion(urlDptos,urlMun,urlPer);
        System.out.println("Departamentos " + s.getListadoDptos());
        System.out.println("Municipios " + s.getListadoMunicipios());
        System.out.println("Personas" + s.getListadoPersonas());
        
    }
    
}
