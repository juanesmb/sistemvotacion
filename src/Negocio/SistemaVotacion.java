package Negocio;
import Entidad.*;
import java.time.LocalDateTime;
import ufps.util.colecciones_seed.*;
import ufps.util.varios.ArchivoLeerURL;
public class SistemaVotacion {
    
    private ListaCD<Departamento> dptos=new ListaCD();
    private ListaCD<Persona> personas=new ListaCD();
    private Cola<Notificacion> notificaciones=new Cola();

    public SistemaVotacion(String urlDptos,String urlMun,String urlPer) {
        crearDptos(urlDptos);
        try{
        crearMunicipios(urlMun);
        }catch(Exception e){
        System.err.println(e.getMessage());
        }
        crearPersonas(urlPer);
    }

    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    public void setDptos(ListaCD<Departamento> dptos) {
        this.dptos = dptos;
    }

    public ListaCD<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ListaCD<Persona> personas) {
        this.personas = personas;
    }

    public Cola<Notificacion> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(Cola<Notificacion> notificaciones) {
        this.notificaciones = notificaciones;
    }

    private void crearDptos(String url) {
        ArchivoLeerURL leer = new ArchivoLeerURL(url);
        Object v[] = leer.leerArchivo();
        
        for(int i = 1;i<v.length;i++)
        {
            String linea = v[i].toString();
            String datos[] = linea.split(";");
            //id_dpto;Nombre Departamento}
            int id = Integer.parseInt(datos[0]);
            String nombre = datos[1];
            Departamento dep = new Departamento(id,nombre);
            this.dptos.insertarAlFinal(dep);
        }
    }
    
    private void crearMunicipios(String urlMun) throws Exception{
        ArchivoLeerURL leer = new ArchivoLeerURL(urlMun);
        Object v[] = leer.leerArchivo();
        
        for(int i = 1;i<v.length;i++)
        {
            String linea = v[i].toString();
            String datos [] = linea.split(";");
            //id_dpto;id_municipio;nombreMunicipio
            int idDpto = Integer.parseInt(datos[0]);
            int idMun = Integer.parseInt(datos[1]);
            String nombre = datos[2];
            //creacion del municipio
            Municipio mun = new Municipio(idMun,nombre);
            
            Departamento dpto = buscarDpto(idDpto);
            if(dpto!=null)
            {
                dpto.getMunicipios().insertarAlFinal(mun);
            }else{
                throw new Exception ("El municipio" + nombre + "no tiene un departamento creado");
            }
        }
    }
    
    private Departamento buscarDpto(int idDpto) {
        for(Departamento dato:this.dptos)
        {
            if(dato.getId_dpto() == idDpto)
                return dato;
        }
        return null;
    }

    private void crearPersonas(String url) {
        ArchivoLeerURL leer= new ArchivoLeerURL(url);
        Object v[] = leer.leerArchivo();
        
        for(int i = 1;i<v.length;i++)
        {
            String linea = v[i].toString();
            String datos[] = linea.split(";");
            //cedula;nombre;fechanacimiento;id_municipio_inscripcion;email NORMALIZACIÓN
            long cedula = Long.parseLong(datos[0]);
            String nombre = datos[1];
            LocalDateTime fecha = crearFecha(datos[2]);
            short idMunicipio = Short.parseShort(datos[3]);
            String email = datos[4];  
            //crear persona
            Persona per = new Persona(cedula,nombre,fecha,idMunicipio,email);
            this.personas.insertarAlFinal(per);
        }
    }

    private LocalDateTime crearFecha(String fecha) {
        String datos[] = fecha.split("-");
        int anio = Integer.parseInt(datos[0]);
        int mes = Integer.parseInt(datos[1]);
        int dia = Integer.parseInt(datos[2]);
        return LocalDateTime.of(anio,mes,dia,0,0,0);
    }

    public String getListadoDptos() {
        String lista = "";
        for (Departamento dato:this.dptos)
        {
            lista = lista + dato.toString()+"\n";
        }
        return lista;
    }

    public String getListadoPersonas() {
        String lista = "";
        for (Persona dato:this.personas)
        {
            lista = lista + dato.toString()+"\n";
        }
        return lista;
    }   

    public String getListadoMunicipios() {
        String municipios = "";
        for(Departamento dpto:this.dptos)
        {
            for(Municipio mun:dpto.getMunicipios())
            {
                municipios += mun.toString() + "\n";
            }
        }
        return municipios;
    }
}
